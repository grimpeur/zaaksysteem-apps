import React from 'react';
import classNames from 'classnames';
import { logHeaderStyleSheet } from './LogHeader.style';
import SubAppHeader from '../../Shared/Header/SubAppHeader';
import Title from '../../Shared/Header/Title';
import ToggleFilterButton from './ToggleFilterButton';
import ExportButton from './ExportButton';
import TextFieldFilter from './Filters/TextFieldFilter';
import SelectFilter from './Filters/SelectFilter';
import {
  withStyles,
} from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {string} props.caseNumberTranslation
 * @param {Function} props.changeFilterValue
 * @param {Function} props.changeFocus
 * @param {Object} props.classes
 * @param {Function} props.clearFilter
 * @param {string} props.exportButtonTitle
 * @param {Function} props.exportLog
 * @param {Function} props.fetchUsers
 * @param {Object} props.filters
 * @param {string} props.filters.keyword
 * @param {string} props.filters.caseNumber
 * @param {Object} props.filters.user
 * @param {string} props.focusedFilter
 * @param {string} props.headerTitle
 * @param {string} props.keywordTranslation
 * @param {Function} props.onTextFieldKeyDown
 * @param {Boolean} props.showFilters
 * @param {Function} props.toggleFilters
 * @param {Object} props.exportParams
 * @param {Array<Object>} props.userOptions
 * @param {Object} props.userTranslations
 */
const LogHeader = ({
  caseNumberTranslation,
  changeFilterValue,
  changeFocus,
  classes,
  clearFilter,
  exportButtonTitle,
  exportLog,
  fetchUsers,
  filters: {
    keyword,
    caseNumber,
    user,
  },
  focusedFilter,
  headerTitle,
  keywordTranslation,
  onTextFieldKeyDown,
  removeFocus,
  showFilters,
  toggleFilters,
  exportParams,
  userOptions,
  userTranslations,
}) => (
  <SubAppHeader>
    <div className={classes.headerWrapper}>
      <Title>
        {headerTitle}
      </Title>
      <ExportButton
        action={exportLog}
        value={exportButtonTitle}
        exportParams={exportParams}
      />
      <ToggleFilterButton
        action={toggleFilters}
        type="search"
      />
    </div>

    <div className={classNames(classes.headerWrapper, classes.filterOverlay, { [classes.hideFilterOverlay]: !showFilters })}>
      <div
        role="presentation"
        onChange={changeFilterValue}
        onFocus={changeFocus}
        onBlur={removeFocus}
        onKeyDown={onTextFieldKeyDown}
      >
        <TextFieldFilter
          name="keyword"
          value={keyword}
          hasFocus={focusedFilter === 'keyword'}
          startAdornmentName="search"
          endAdornmentAction={clearFilter}
          placeholder={keywordTranslation}
        />
        <TextFieldFilter
          name="caseNumber"
          value={caseNumber}
          hasFocus={focusedFilter === 'caseNumber'}
          startAdornmentName="folder"
          endAdornmentAction={clearFilter}
          placeholder={caseNumberTranslation}
        />
        <SelectFilter
          name="user"
          value={user}
          hasFocus={focusedFilter === 'user'}
          translations={userTranslations}
          changeFilterValue={changeFilterValue}
          changeFocus={changeFocus}
          removeFocus={removeFocus}
          fetchUsers={fetchUsers}
          startAdornmentName="person"
          userOptions={userOptions}
          userTranslations={userTranslations}
        />
      </div>
      <ExportButton
        action={exportLog}
        value={exportButtonTitle}
        exportParams={exportParams}
      />
      <ToggleFilterButton
        action={toggleFilters}
        type="close"
      />
    </div>
  </SubAppHeader>
);

export default withStyles(logHeaderStyleSheet)(LogHeader);
