const DISPLAYSHORT = 2;
const MONTHCORRECTION = 1;

/**
 * @param value
 * @return {string}
 */
const padDate = value =>
  value
    .toString()
    .padStart(DISPLAYSHORT, '0');

/**
 * @param date
 * @return {Date}
 */
const getDate = date =>
  new Date(date);

/**
 * @param iso
 * @return {string}
 */
export const createDate = iso => {
  const date = getDate(iso);
  const year = date.getFullYear();
  const month = padDate(date.getMonth() + MONTHCORRECTION);
  const day = padDate(date.getDate());

  return `${day}-${month}-${year}`;
};

/**
 * @param iso
 * @return {string}
 */
export const createTime = iso => {
  const date = getDate(iso);
  const hours = padDate(date.getHours());
  const minutes = padDate(date.getMinutes());
  const seconds = padDate(date.getSeconds());

  return `${hours}:${minutes}:${seconds}`;
};
