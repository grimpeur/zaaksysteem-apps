const { spawn } = require('child_process');
const { NODE_ROOT } = require('./constants');

const EXIT_CODE_OK = 0;

/**
 * @param {string} binary
 * @param {Array<string>} [argumentList=[]]
 * @param {string} [cwd=NODE_ROOT]
 */
const exec = (binary, argumentList = [], cwd = NODE_ROOT) =>
  new Promise(function executor(resolve, reject) {
    spawn(binary, argumentList, {
      cwd,
      stdio: 'inherit',
    })
      .on('close', function onClose(exitCode) {
        if (exitCode === EXIT_CODE_OK) {
          resolve(exitCode);
        } else {
          reject(exitCode);
        }
      });
  });

module.exports = exec;
