# Reducer

## Reduce (pun intended) Boilerplate

The `createReducer` utility function takes the 
initial state and a dictionary object that 
associates action creators with reducer functions.

### Example

    import { createReducer } from '../../../library/redux/createReducer';
    import action from '../Action';
    
    const { pause, resume } = action.takeFive;
    const initialState = false;
    
    export const takeFiveReducer = createReducer(initialState, {
      [pause]: () => true,
      [resume]: () => initialState,
    });
    