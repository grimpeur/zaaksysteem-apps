import { asArray } from '../../library/array';

/**
 * @param {Function} dispatch
 *   Redux store dispatcher
 * @return {Function}
 *   Promise fulfillment handler
 */
const fulfillmentFactory = dispatch =>
  function onFulfilled(actions) {
    for (const action of asArray(actions)) {
      if (typeof action === 'function') {
        dispatch(action());
      }
    }
  };


/**
 * Factory function for creating a simple side effetcs middleware for redux.
 *
 * @param {Object} sideEffects
 *   An object with property names that correspond to action types.
 * @return {Function}
 */
export const reduxSideEffectsMiddlewareFactory = sideEffects =>
  store =>
    next =>
      action => {
        const { payload, type } = action;

        if (sideEffects.hasOwnProperty(type)) {
          const { dispatch } = store;
          const onFulfilled = fulfillmentFactory(dispatch);

          for (const sideEffect of sideEffects[type]) {
            sideEffect(payload, store)
              .then(onFulfilled);
          }
        }

        return next(action);
      };
