import React from 'react';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Object} props.value
 * @return {ReactComponent}
 */
export const UserCell = ({
  classes,
  value,
}) => (
  <div>
    <span className={classes.displayName}>
      {value.displayName}
    </span>
  </div>
);

export default withStyles(cellStyleSheet)(UserCell);
