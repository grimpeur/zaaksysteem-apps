import get  from './get';

const { entries, keys } = Object;

/**
 * @param {Object|string} value
 * @return {boolean}
 */
const isObject = value => (typeof value !== 'string');

/**
 * @param {Array} array
 * @return {Object}
 */
function getObject(array) {
  const [object, invalid] = array.filter(value => isObject(value));

  if (object === undefined) {
    throw new TypeError('no object');
  }

  if (invalid !== undefined) {
    throw new TypeError('multiple objects');
  }

  return object;
}

/**
 * @param {Array} rest
 * @return {Object}
 */
export function purge(...rest) {
  const object = getObject(rest);

  return entries(object)
    .reduce((accumulator, [key, value]) => {
      if (!rest.includes(key)) {
        accumulator[key] = value;
      }

      return accumulator;
    }, {});
}

/**
 * @param rest
 * @return {Array}
 */
export function extract(...rest) {
  const object = getObject(rest);

  const map = value => {
    if (value === object) {
      return purge(...rest);
    }

    return object[value];
  };

  return rest.map(map);
}

/**
 * @param {Object} object
 * @param {Object} paramGetters
 * @return {Object}
 */
export function performGetOnProperties(object, paramGetters) {
  return keys(object).reduce((acc, param) => {
    acc[param] = get(object, paramGetters[param]);

    return acc;
  }, {});
}

/**
 * @param {Object} object
 * @param rest
 * @return {Object}
 */
export function filterProperties(object, ...rest) {
  return rest.reduce(function addProperty(accumulator, x) {
    if (object.hasOwnProperty(x)) {
      accumulator[x] = object[x];
    }

    return accumulator;
  }, {});
}
