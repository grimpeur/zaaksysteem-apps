import {
  buildParamString,
  buildUrl,
  getSegment,
  getUrl,
  navigate,
  objectifyParams,
  parseUrl,
} from './url';

describe('The `url` module', () => {
  describe('exports a `getSegment` function that', () => {
    test('returns the second segment of a given path component', () => {
      const actual = getSegment('/foo/bar/baz');
      const expected = 'bar';

      expect(actual).toBe(expected);
    });

    test('returns the second segment of a given path and query component', () => {
      const actual = getSegment('/foo/bar?baz');
      const expected = 'bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports an `getUrl` function for the window location object that', () => {
    test('returns the path component when a fragment identifier is present', () => {
      window.location.assign('/foo#bar');

      const actual = getUrl();
      const expected = '/foo';

      expect(actual).toBe(expected);
    });

    test('returns the path and query components', () => {
      window.location.assign('/foo?bar');

      const actual = getUrl();
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `navigate` function that', () => {
    test('cannot be tested with jest', () => {
      window.location.assign('/foo');

      navigate('/bar');

      const actual = getUrl();
      const expected = '/bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports an `parseUrl` function for a string that', () => {
    test('returns the path and query components', () => {
      const actual = parseUrl('https://example.org/foo?bar#quux');
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });

    test('strips a trailing slash', () => {
      const actual = parseUrl('https://example.org/foo/');
      const expected = '/foo';

      expect(actual).toBe(expected);
    });

    test('strips a trailing slash', () => {
      const actual = parseUrl('https://example.org/foo/?bar');
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `buildParamString` function that', () => {
    test('returns the params of an object as a string with ?-prefix', () => {
      const params = {
        one: 'first',
        two: 'second',
        three: 'third',
      };
      const actual = buildParamString(params);
      const expected = '?one=first&two=second&three=third';

      expect(actual).toBe(expected);
    });

    test('returns an empty string for an empty object', () => {
      const params = {};
      const actual = buildParamString(params);
      const expected = '';

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `buildUrl` function for a string that', () => {
    const url = '/api/v1/test';

    test('returns the given path with params', () => {
      const params = {
        one: 'first',
        two: 'second',
        three: 'third',
      };
      const actual = buildUrl(url, params);
      const expected = `${url}?one=first&two=second&three=third`;

      expect(actual).toBe(expected);
    });

    test('returns the given path with encoded params', () => {
      const params = {
        one: 'f&irst',
        two: 's:econd',
        three: 't?hird',
      };
      const actual = buildUrl(url, params);
      const expected = `${url}?one=f%26irst&two=s%3Aecond&three=t%3Fhird`;

      expect(actual).toBe(expected);
    });

    test('returns the given path with params that have a value', () => {
      const params = {
        one: 'first',
        two: '',
        three: null,
        four: 'fourth',
      };
      const actual = buildUrl(url, params);
      const expected = `${url}?one=first&four=fourth`;

      expect(actual).toBe(expected);
    });
  });

  /**
   * @test {extract}
   */
  describe('exports an `objectifyParams` function that', () => {
    test('transforms a URL string of params to an object with key value pairs', () => {
      const paramString = 'foo=FOO&bar=BAR&quux=QUUX';
      const actual = objectifyParams(paramString);
      const expected = {
        foo: 'FOO',
        bar: 'BAR',
        quux: 'QUUX',
      };

      expect(actual).toEqual(expected);
    });
  });
});
