#! /usr/bin/env node
/***********************************************************************
 * Build the vendor bundles. Current scope:
 * - React universe
 * - Mintlab UI
 * - Mintlab IE11 polyfill (just a cp of the distribution)
 ***********************************************************************/
const { copyFileSync, existsSync } = require('fs');
const { join } = require('path');
const webpack = require('webpack');
const ConcatPlugin = require('webpack-concat-plugin');
const { sync: globSync } = require('glob');
const { read } = require('./library/file');
const { info } = require('./library/methods');
const { removeChildren } = require('./library/remove');
const {
  ENVIRONMENT,
  SERVER_DOCUMENT_ROOT,
  VENDOR_PACKAGE_PATH,
  VENDOR_SERVER_PATH,
} = require('./library/constants');
const {
  onWebpackComplete,
} = require('./library/methods');
const {
  dllPluginFactory,
  dllReferencePluginFactory,
  library,
} = require('./library/webpack/dll');

const { dll } = read(`${VENDOR_PACKAGE_PATH}/package.json`);
const assetPath = join(SERVER_DOCUMENT_ROOT, 'vendor');

/**
 * React DLL bundle dependencies.
 *
 * @type {Array}
 */
const react = dll['react:redux'];

const scope = '@mintlab';
const packageName = 'ui';

/**
 * Mintlab DLL bundle dependencies.
 *
 * @type {Array}
 */
const ui = [
  `${scope}/${packageName}`,
];

/**
 * Webpack output filename pattern.
 * ZS-TODO: use hash for filename
 *
 * @type {string}
 */
const filename = '[name]-[hash].js';

/**
 * @param {Object} entry
 * @param {Object} plugins
 * @return {Object}
 */
const configurationFactory = ({ entry, module, plugins }) => ({
  entry,
  output: {
    filename,
    library,
    path: VENDOR_SERVER_PATH,
  },
  mode: ENVIRONMENT,
  module,
  performance: {
    maxAssetSize: 400000,
    maxEntrypointSize: 400000,
  },
  plugins,
  resolve: {
    modules: [
      join(VENDOR_PACKAGE_PATH, 'node_modules'),
    ],
  },
});

const reactConfiguration = configurationFactory({
  entry: {
    react,
  },
  module: {
    rules: [
      {
        include: [
          /\/node_modules\/@mintlab\/kitchen-sink\//,
        ],
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                "@babel/preset-env",
              ],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    dllPluginFactory(),
  ],
});

/**
 * The DLL bundle build loses the `import()` chunks.
 */
function copyOrphanChunks() {
  const distribution = join(VENDOR_PACKAGE_PATH, 'node_modules', scope, packageName, 'distribution');
  const orphans = globSync('ui.*.*.js', {
    cwd: distribution,
  });

  const queue = orphans
    .map(baseName => [
      join(distribution, baseName),
      join(VENDOR_SERVER_PATH, baseName),
    ]);

  for (const [from, to] of queue) {
    if (existsSync(from)) {
      copyFileSync(from, to);
    }
  }
}

/**
 * ZS-TODO: sequential build
 * Implement a more elegant way to chain
 * multiple builds that depend on each other.
 *
 * @param {Array} rest
 */
function reactCallback(...rest) {
  onWebpackComplete(...rest);
  copyOrphanChunks();

  const uiConfiguration = configurationFactory({
    entry: {
      ui,
    },
    plugins: [
      dllReferencePluginFactory('react'),
      dllPluginFactory(),
      // Because the polyfills monkey-patch the global environment,
      // we don't want the useless webpack JSONP wrapper here.
      // ZS-FIXME: this plugin spawns a deprecation warning
      // https://github.com/hxlniada/webpack-concat-plugin/issues/58
      new ConcatPlugin({
        fileName: '[name]-[hash:20].js',
        filesToConcat: ['@mintlab/ie11'],
        name: 'ie11',
        uglify: false,
      }),
    ],
  });

  webpack(uiConfiguration, onWebpackComplete);
}


info('Removing existing vendor assets.');
removeChildren(assetPath);

info('Building the vendor assets.');
webpack(reactConfiguration, reactCallback);
