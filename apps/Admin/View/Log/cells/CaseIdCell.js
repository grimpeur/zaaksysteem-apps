import React from 'react';
import { Render } from '@mintlab/ui';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.value
 * @return {ReactComponent}
 */
export const CaseIdCell = ({
  classes,
  value,
}) => (
  <Render
    condition={value}
  >
    <a
      className={classes.link}
      href={`/intern/zaak/${value}`}
    >{value}</a>
  </Render>
);

export default withStyles(cellStyleSheet)(CaseIdCell);
