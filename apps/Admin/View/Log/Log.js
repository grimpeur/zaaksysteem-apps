import React, { Component } from 'react';
import { logStyleSheet } from './Log.style';
import LogHeader from './header/LogHeader';
import { sharedStylesheet } from '../Shared/Shared.style';
import {
  withStyles,
  Pagination,
  Table,
  Card,
} from '@mintlab/ui';
import {
  bind,
} from '@mintlab/kitchen-sink';
import deepmerge from 'deepmerge';
import deepEqual from 'fast-deep-equal';
import get from '../../../library/get';
import { filterProperties } from '../../../library/object';

const {
  assign,
  values,
  keys,
} = Object;

const filterDefaults = {
  keyword: '',
  caseNumber: '',
  user: null,
};

const watchedProps = [
  'page',
  'rowsPerPage',
  'params',
];

/**
 * @param {Object} prevProps
 * @param {Object} currProps
 * @return {Boolean}
 */
const compareTriggerProps = (prevProps, currProps) => {
  const previousProps = filterProperties(prevProps, ...watchedProps);
  const currentProps = filterProperties(currProps, ...watchedProps);

  return !deepEqual(previousProps, currentProps);
};

/**
 * @reactProps {Object} classes
 * @reactProps {string} caseNumberTranslation
 * @reactProps {Array} columns
 * @reactProps {number} count
 * @reactProps {string} exportButtonTitle
 * @reactProps {Function} exportLog
 * @reactProps {Function} fetchUsers
 * @reactProps {Object} keywordTranslation
 * @reactProps {string} headerTitle
 * @reactProps {string} labelRowsPerPage
 * @reactProps {string} noResultDescription
 * @reactProps {number} page
 * @reactProps {Object} params
 * @reactProps {number} rowsPerPage
 * @reactProps {Array<Object>} rows
 * @reactProps {Array<number>} rowsPerPageOptions
 * @reactProps {Array<Object>} userOptions
 * @reactProps {Object} userTranslations
 */
export class Log extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const { params } = props;

    this.state = {
      focusedFilter: undefined,
      showFilters: keys(params).length,
      filters: keys(params).length ? params : filterDefaults,
      userOptions: [],
    };

    bind(this,
      'getNewPage',
      'changeRowsPerPage',
      'changeFilterValue',
      'clearFilter',
      'toggleFilters',
      'changeFocus',
      'removeFocus',
      'onTextFieldKeyDown'
    );
  }

  componentDidMount() {
    const {
      fetchUserByUuid,
      fetchNewData,
    } = this.props;

    fetchNewData();

    if(typeof this.state.filters.user === 'string') {
      fetchUserByUuid();
    }
  }

  /**
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    const propsChanged = compareTriggerProps(prevProps, this.props);

    if (propsChanged) {
      this.props.fetchNewData();
    }

    if(prevProps.initialUser !== this.props.initialUser) {
      const filters = assign({}, this.state.filters, {user: this.props.initialUser});

      this.setState({filters});
    }
  }

  /**
   * @return {ReactComponent}
   */
  render() {
    const {
      props: {
        classes,
        caseNumberTranslation,
        columns,
        count,
        exportButtonTitle,
        exportLog,
        fetchUsers,
        keywordTranslation,
        headerTitle,
        labelRowsPerPage,
        noResultDescription,
        page,
        params,
        rowsPerPage,
        rows,
        rowsPerPageOptions,
        userOptions,
        userTranslations,
      },
      state: {
        showFilters,
        filters,
        focusedFilter,
      },
    } = this;

    return (
      <div className={classes.wrapper}>
        <LogHeader
          caseNumberTranslation={caseNumberTranslation}
          exportButtonTitle={exportButtonTitle}
          exportLog={exportLog}
          fetchUsers={fetchUsers}
          headerTitle={headerTitle}
          keywordTranslation={keywordTranslation}
          userTranslations={userTranslations}
          userOptions={userOptions}
          showFilters={showFilters}
          filters={filters}
          focusedFilter={focusedFilter}
          changeFilterValue={this.changeFilterValue}
          changeFocus={this.changeFocus}
          removeFocus={this.removeFocus}
          onTextFieldKeyDown={this.onTextFieldKeyDown}
          clearFilter={this.clearFilter}
          exportParams={params}
          toggleFilters={this.toggleFilters}
        />

        <div className={classes.sheet}>
          <Card className={classes.tableWrapper}>
            <Table
              classes={classes}
              columns={columns}
              noResultDescription={noResultDescription}
              page={page}
              rows={rows}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={rowsPerPageOptions}
            />
          </Card>
          <Pagination
            changeRowsPerPage={this.changeRowsPerPage}
            component={'div'}
            count={count}
            getNewPage={this.getNewPage}
            labelDisplayedRows={() => {
            }}
            labelRowsPerPage={labelRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={rowsPerPageOptions}
          />
        </div>
      </div>
    );
  }

  /**
   * @param {number} nextPage
   *   Synthetic event.
   */
  getNewPage(nextPage) {
    this.props.invokeRoute({
      nextPage,
      nextParams: this.state.filters,
    });
  }

  /**
   * @param {Object} event
   *   Synthetic event.
   */
  changeRowsPerPage(event) {
    const nextRowsPerPage = get(event, 'target.value');
    const {
      getPageByRows,
      invokeRoute,
      page,
      rowsPerPage,
    } = this.props;
    const nextPage = getPageByRows(page, rowsPerPage, nextRowsPerPage);

    invokeRoute({
      nextPage,
      nextRowsPerPage,
      nextParams: this.state.filters,
    });
  }

  /**
   * @param {*} param
   *   Synthetic event.
   */
  changeFilterValue(param) {
    const { name, value } = get(param, 'target', param);
    const filters = assign({}, this.props.params, {user: this.state.filters.user}, {[name]: value});

    this.setState({filters});

    if(name === 'user') {
      this.props.invokeRoute({
        nextPage: 0,
        nextParams: filters,
      });
    }
  }

  /**
   * @param {Object} keyPressEvent
   *   Synthetic event.
   */
  onTextFieldKeyDown(keyPressEvent) {
    const { name, value } = keyPressEvent.target;

    if(keyPressEvent.key === 'Enter') {
      this.props.invokeRoute({
        nextPage: 0,
        nextParams: assign({}, this.props.params, {user: this.state.filters.user}, {[name]: value}),
      });
    }
  }

  /**
   * @param {string} name
   *   Synthetic event.
   */
  clearFilter(name) {
    const {
      state: {
        filters,
      },
    } = this;

    this.setState({
      filters: assign({}, filters, {[name]: filterDefaults[name]}),
    });

    this.props.invokeRoute({
      nextPage: 0,
      nextParams: assign({}, this.props.params, {user: this.state.filters.user}, {[name]: filterDefaults[name]}),
    });
  }

  /**
   * toggles the filter bar and resets the filters to default
   */
  toggleFilters() {
    const hasParams =
      values(this.props.params)
        .some(val => val);

    const updatedState = {
      showFilters: !this.state.showFilters,
    };

    if(hasParams) {
      updatedState.filters = filterDefaults;
    }

    this.setState(updatedState);

    if(hasParams) {
      this.props.invokeRoute({
        nextPage: 0,
        nextParams: filterDefaults,
      });
    }
  }

  /**
   *   Synthetic event.
   */
  changeFocus(param) {
    this.setState({
      focusedFilter: param.target.name,
    });
  }

  /**
   *   Synthetic event.
   */
  removeFocus() {
    this.setState({
      focusedFilter: undefined,
    });
  }
}

/**
 * @param {Object} theme
 * @return {JSS}
 */
const mergedStyles = theme =>
  deepmerge(
    sharedStylesheet(theme),
    logStyleSheet(theme)
  );

export default withStyles(mergedStyles)(Log);
