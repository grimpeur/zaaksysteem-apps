import { connect } from 'react-redux';
import Login from './Login';
import action from '../../../Store/Action';

const mapDispatchToProps = dispatch => ({
  route(path) {
    const { invoke } = action.route;

    dispatch(invoke(path));
  },
});

const LoginContainer = connect(null, mapDispatchToProps)(Login);

export default LoginContainer;
