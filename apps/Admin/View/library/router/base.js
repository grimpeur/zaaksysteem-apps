/*👀
 * Re-export the stable configuration for base segment routing.
 * This module can be swapped with `base.edge` by the
 * `NormalModuleReplacementPlugin`.
 */
export { default } from './base.iframe';
