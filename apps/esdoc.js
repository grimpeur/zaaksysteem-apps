/**
 * ESDoc types with external documentation.
 * This file is found automatically as long as it is located under
 * the `source` path in the ESDoc configuration.
 */

// React

/**
 * @external {ReactElement} https://github.com/facebook/react/blob/master/packages/react/src/ReactElement.js#L111
 */

/**
 * @external {RefObject} https://github.com/facebook/react/blob/master/packages/shared/ReactTypes.js#L78
 */

/**
 * @external {React.Children} https://reactjs.org/docs/react-api.html#reactchildren
 */

/**
 * @external {React.Component} https://reactjs.org/docs/react-api.html#reactcomponent
 */

/**
 * @external {React.Fragment} https://reactjs.org/docs/react-api.html#reactfragment
 */

/**
 * @external {React.PureComponent} https://reactjs.org/docs/react-api.html#reactpurecomponent
 */

// DOM

/**
 * @external {Node} https://developer.mozilla.org/en-US/docs/Web/API/Node
 */

/**
 * @external {Event} https://developer.mozilla.org/en-US/docs/Web/API/Event
 */

/**
 * @external {MutationObserver} https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
 */

// Other

/**
 * @external {i18next} https://www.i18next.com/
 */
