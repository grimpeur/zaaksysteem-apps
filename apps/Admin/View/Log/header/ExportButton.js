import React from 'react';
import { exportButtonStyleSheet } from './ExportButton.style';
import {
  withStyles,
  Button,
} from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.action
 * @param {Object} props.exportParams
 * @param {string} props.value
 * @return {ReactComponent}
 */
export const ExportButton = ({
  classes,
  action,
  exportParams,
  value,
}) => (
  <div className={classes.wrapper}>
    <Button
      action={() => action(exportParams)}
      presets={['secondary', 'contained']}
    >
      {value}
    </Button>
  </div>
);

export default withStyles(exportButtonStyleSheet)(ExportButton);
