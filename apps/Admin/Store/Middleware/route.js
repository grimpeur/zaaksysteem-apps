import actions from '../Action/index';
import { fieldsChanged } from '../../../library/form';
import get from '../../../library/get';
import { getSegment } from '../../../library/url';

const {
  route: {
    invoke,
  },
  ui: {
    dialog: {
      show,
    },
  },
} = actions;

/**
 * @param {Object} options
 * @param {string} options.current
 * @param {string} options.next
 * @param {string} options.segment
 * @return {boolean}
 */
const willSegmentChange = ({ current, next, segment }) => (
  (getSegment(current) === segment)
  && getSegment(next) !== segment
);

/**
 * @param {Object} form
 * @param {Array} items
 * @return {boolean|number}
 */
const didFieldsChange = (form, items) =>
  Boolean(fieldsChanged(form, items).length);

/**
 * @param {Object} store
 * @param {Object} payload
 * @return {boolean}
 */
function didDialogActionDispatch(store, payload) {
  const state = store.getState();
  const isConfigurationExit = willSegmentChange({
    current: state.route,
    next: payload.path,
    segment: 'configuratie',
  });

  if (isConfigurationExit) {
    const { dispatch } = store;
    const form = get(state, 'form');
    const items = get(state, 'resource.config.data.items');

    if (didFieldsChange(form, items)) {
      dispatch(show({
        type: 'DiscardChanges',
        options: payload,
      }));

      return true;
    }
  }

  return false;
}

/**
 * @param {string} type
 * @param {boolean|undefined} force
 * @return {boolean}
 */
const isInterceptableRouteAction = (type, force) => (
  (type === String(invoke))
  && !force
);

/**
 * Intercept the route 'resolve' action.
 * If any fields have been changed but not yet saved, cancel this action
 * and open a DiscardChanges dialog
 */
const route = store =>
  next =>
    action => {
      const { type, payload } = action;
      const isIntercepted = (
        isInterceptableRouteAction(type, get(payload, 'force'))
        && didDialogActionDispatch(store, payload)
      );

      if (!isIntercepted) {
        return next(action);
      }
    };

export default route;
