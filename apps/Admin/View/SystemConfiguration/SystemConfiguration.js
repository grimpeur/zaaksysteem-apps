import React from 'react';
import Categories from './Categories/Categories';
import FormWrapper from './FormWrapper/FormWrapper';
import { systemConfigurationStyleSheet } from './SystemConfiguration.style.js';
import SubAppHeader from '../Shared/Header/SubAppHeader';
import Title from '../Shared/Header/Title';
import { withStyles } from '@mintlab/ui';
import SnackbarContainer from '../Shared/Snackbar/SnackbarContainer';
import DialogContainer from '../Shared/Dialog/DialogContainer';

/**
 * @param {Object} props
 * @param {Function} props.invoke
 * @param {Array} props.categories
 * @param {Function} props.set
 * @param {Array} props.fieldSets
 * @param {Function} props.t
 * @param {String} props.identifier
 * @param {Object} props.classes
 * @param {Array} props.changed
 * @param {Object} props.banners
 * @param {Function} props.showBanner
 * @param {Function} props.hideBanner
 * @param {Function} props.showDialog
 * @param {Function} props.request
 * @return {ReactElement}
 */
export const SystemConfiguration = ({
  invoke,
  categories,
  set,
  fieldSets,
  t,
  identifier,
  classes,
  changed,
  banners,
  showBanner,
  hideBanner,
  showDialog,
  request,
}) => (
  <div className={classes.wrapper}>

    <SubAppHeader>
      <Title>
        {t('systemConfiguration:title')}
      </Title>
    </SubAppHeader>

    <section className={classes.grid}>
      <Categories
        categories={categories}
        invoke={invoke}
      />

      <FormWrapper
        fieldSets={fieldSets}
        changed={changed}
        set={set}
        request={request}
        t={t}
        banners={banners}
        identifier={identifier}
        showBanner={showBanner}
        hideBanner={hideBanner}
        showDialog={showDialog}
      />
    </section>

    <DialogContainer/>
    <SnackbarContainer/>
    
  </div>
);

export default withStyles(systemConfigurationStyleSheet)(SystemConfiguration);
