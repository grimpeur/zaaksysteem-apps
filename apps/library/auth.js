import { navigate } from './url';

/*
 * Authentication and authorization utilities.
 */

const AUTH_PATH = '/auth';
export const LOGIN_PATH = [AUTH_PATH, 'login'].join('/');
export const LOGOUT_PATH = [AUTH_PATH, 'logout'].join('/');

/**
 * @param {Object} session
 * @return {boolean}
 */
export const isLoggedIn = sessionData =>
  Boolean(sessionData.logged_in_user);

/**
 * Redirect the current user to the login page.
 *
 * @param {string} referer
 */
export function login(referer) {
  navigate(`${LOGIN_PATH}?referer=${referer}`);
}

/**
 * Redirect the current user to the logout page.
 */
export function logout() {
  navigate(LOGOUT_PATH);
}

const ADMIN_CAPABILITY = 'admin';

/**
 * @param {Array<string>} userCapabilities
 * @param {string} capability
 * @return {boolean}
 */
const isAuthorized = (userCapabilities, capability) => (
  userCapabilities.includes(ADMIN_CAPABILITY)
  || userCapabilities.includes(capability)
);

/**
 * Filter the generic user navigation data objects on a
 * `capability` property that is included in `userCapabilities`.
 *
 * @param {Array<Object>} navigationData
 * @param {Array<string>} userCapabilities
 * @return {Array<Object>}
 */
export const getUserNavigation = (navigationData, userCapabilities) =>
  navigationData
    .filter(({
      capability,
    }) =>
      isAuthorized(userCapabilities, capability));
