import React from 'react';
import { subAppHeaderStylesheet } from './SubAppHeader.style.js';
import { withStyles } from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const SubAppHeader = ({
  children,
  classes,
}) => (
  <header
    className={classes.header}
  >
    { children }
  </header>
);

export default withStyles(subAppHeaderStylesheet)(SubAppHeader);
