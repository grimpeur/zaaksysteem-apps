import React from 'react';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';

export const ComponentCell = ({
  classes,
  value,
}) => (
  <span
    className={classes.component}
  >{value}</span>
);

export default withStyles(cellStyleSheet)(ComponentCell);
