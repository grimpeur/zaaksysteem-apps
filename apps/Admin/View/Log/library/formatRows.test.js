import formatRows from './formatRows';

describe('The `formatRows` function maps the', () => {
  const sample = () => ({
    caseId: 42,
    component: 'case',
    date: '2018-12-19T08:48:44Z',
    description: 'Hello, world',
    user: {
      displayName: 'Fred Foobar',
    },
  });
  const [value] = formatRows([sample()]);

  test('`caseId` configuration value to the `value` prop of a component', () => {
    expect(value.caseId.props).toEqual({
      value: sample().caseId,
    });
  });

  test('`component` configuration value to the `value` prop of a component', () => {
    const [value] = formatRows([sample()]);

    expect(value.component.props).toEqual({
      value: sample().component,
    });
  });

  test('`component` configuration value to the `value` prop of a component with translations', () => {
    const componentTranslations = {
      case: 'Zaak',
    };
    const [value] = formatRows([sample()], componentTranslations);

    expect(value.component.props).toEqual({
      value: componentTranslations[sample().component],
    });
  });

  test('`date` configuration value to the `value` prop of a component', () => {
    const [value] = formatRows([sample()]);

    expect(value.date.props).toEqual({
      value: sample().date,
    });
  });

  test('`description` configuration value to the `children` prop of a component', () => {
    const [value] = formatRows([sample()]);

    expect(value.description.props).toEqual({
      children: sample().description,
    });
  });

  test('`user` configuration value to the `value` prop of a component', () => {
    const [value] = formatRows([sample()]);

    expect(value.user.props).toEqual({
      value: sample().user,
    });
  });
});
