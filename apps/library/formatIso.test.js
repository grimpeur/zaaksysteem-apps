import { createDate, createTime } from './formatIso';

/**
 * @test {createDate}
 */
describe ('The `formatIso` module exports a function createDate that', () => {
  test('returns only the dutch formatted date from an dateTimeIso string', () => {
    const iso = '2018-01-13T10:58:59Z';
    const actual = createDate(iso);
    const expected = '13-01-2018';

    expect(actual).toBe(expected);
  });
});

/**
 * @test {createTime}
 */
describe ('The `formatIso` module exports a function createTime that', () => {
  test('returns only the dutch formatted time from an dateTimeIso string', () => {
    const iso = '2018-01-13T10:58:59Z';
    const actual = createTime(iso);
    const expected = '10:58:59';

    expect(actual).toBe(expected);
  });
});
