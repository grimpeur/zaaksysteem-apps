/**
 * Array of navigation link configurations with the signature
 * - `{string}` capability
 * - `{string}` icon
 * - `{string}` label
 * - `{string}` path
 *
 * @type {Array<Object>}
 */
export const navigation = [
  {
    capability: 'beheer_zaaktype_admin',
    icon: 'chrome_reader_mode',
    label: 'Catalogus',
    path: '/admin/catalogus',
  },
  {
    capability: 'useradmin',
    icon: 'people',
    label: 'Gebruikers',
    path: '/admin/gebruikers',
  },
  {
    capability: 'admin',
    icon: 'import_contacts',
    label: 'Logboek',
    path: '/admin/logboek',
  },
  {
    capability: 'admin',
    icon: 'poll',
    label: 'Transacties',
    path: '/admin/transacties',
  },
  {
    capability: 'admin',
    icon: 'all_inclusive',
    label: 'Koppelingen',
    path: '/admin/koppelingen',
  },
  {
    capability: 'admin',
    icon: 'view_comfy',
    label: 'Gegevens',
    path: '/admin/gegevens',
  },
  {
    capability: 'admin',
    icon: 'settings',
    label: 'Configuratie',
    path: '/admin/configuratie',
  },
];
