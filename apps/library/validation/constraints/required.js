import { traverseMap } from '../../../library/map';
import { isDefined, hasValue } from '../../../library/value';

/**
 * @param {*} userInput
 * @return {boolean}
 */
export const required = userInput => {
  const type = (Array.isArray(userInput)) ? 'array' : typeof userInput;

  const map = new Map([
    [type => type === 'string', () => {
      //ZS-TODO: find a reliable way to do this without document
      let tmp = document.createElement('div');
      tmp.innerHTML = userInput;
      const input = tmp.textContent || tmp.innerText;
      return (isDefined(input) && hasValue(input.trim()));
    }],
    [type => type === 'array', userInput => Boolean(userInput.length)],
    [type => type === 'boolean', userInput => hasValue(userInput)],
  ]);

  return traverseMap({
    map,
    keyArgs: [type],
    functionArgs: [userInput],
    fallback: Boolean(userInput),
  });
};
