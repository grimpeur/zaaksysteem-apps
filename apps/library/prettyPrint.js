/**
 * Join multiple strings with a space. This is useful for
 * keeping long pieces of prose in source code readable
 * and indented with their context.
 *
 * @param {...string} subStrings
 * @return {string}
 */
export const multiLine = (...subStrings) => subStrings.join(' ');
