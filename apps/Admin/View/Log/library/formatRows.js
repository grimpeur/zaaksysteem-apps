import { createElement } from 'react';
import createDictionary from '../../../../library/dictionary';
import get from '../../../../library/get';
import {
  CaseIdCell,
  DateCell,
  ComponentCell,
  UserCell,
} from '../cells';

const { keys } = Object;

const dictionary = createDictionary({
  caseId: CaseIdCell,
  date: DateCell,
  component: ComponentCell,
  user: UserCell,
});

/**
 * @param {number} key
 * @param {string} value
 * @return {ReactComponent}
 */
export const columnFormats = (key, value) => {
  if (dictionary[key]) {
    return createElement(dictionary[key], {
      value,
    });
  }

  return createElement('span', null, value);
};

/**
 * @param {Array} rows
 * @param {Object} componentTranslations
 * @return {Array}
 */
export const formatRows = (rows, componentTranslations) =>
  rows
    .map(row => {
      const cells = {};

      keys(row)
        .forEach(key => {
          let value = get(row, key);

          if(key === 'component' && componentTranslations) {
            value = get(componentTranslations, value, null);
          }

          cells[key] = columnFormats(key, value);
        });

      return cells;
    });

export default formatRows;
