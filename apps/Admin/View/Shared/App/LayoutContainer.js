import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import Layout from './Layout';
import action from '../../../Store/Action';

const mapStateToProps = ({
  ui: {
    drawer,
    iframe: {
      loading,
      overlay,
    },
    banners,
  },
  route,
}) => ({
  hasIframeOverlay: overlay,
  isDrawerOpen: drawer,
  isIframeLoading: loading,
  requestUrl: route,
  banners,
});

function getIframeDispatchers(dispatch) {
  const {
    overlay: {
      open,
      close,
    },
    window: {
      load,
      unload,
    },
  } = action.ui.iframe;

  return {
    onIframeOpen: () => dispatch(open()),
    onIframeClose: () => dispatch(close()),
    onIframeLoad: () => dispatch(load()),
    onIframeUnload: () => dispatch(unload()),
  };
}

function mergeProps(stateProps, { dispatch }, ownProps) {
  return {
    ...stateProps,
    ...ownProps,
    ...getIframeDispatchers(dispatch),
    toggleDrawer() {
      const { close, open } = action.ui.drawer;

      if (stateProps.isDrawerOpen) {
        dispatch(close());
      } else {
        dispatch(open());
      }
    },
  };
}

const connectWithTranslation = translate();
const connectWithStore = connect(mapStateToProps, null, mergeProps);

/**
 * The Layout Container Component is connected
 * with the store and the translations.
 *
 * @type {Function}
 */
const LayoutContainer = connectWithTranslation(connectWithStore(Layout));

export default LayoutContainer;
