import get from './get';

const { keys} = Object;

/**
 * Get the second path segment from an absolute path with an optional query.
 *
 * @param {string} path
 * @return {string}
 */
export function getSegment(path) {
  const [pathComponent] = path.split('?');
  const [, , segment] = pathComponent.split('/');

  return segment;
}

/**
 * Get the combined path and query components
 * from a window's location object.
 *
 * @param {Window} [contextWindow = window]
 * @return {string}
 */
export function getUrl(contextWindow = window) {
  const { href } = contextWindow.location;

  return parseUrl(href);
}

/**
 * @param {string} url
 * @param {Window} [contextWindow=window]
 */
export function navigate(url, contextWindow = window) {
  contextWindow.location.assign(url);
}

/**
 * Get the combined path and query components from a URI.
 *
 * @param {string} uri
 * @return {string}
 */
export function parseUrl(uri) {
  const { pathname, search } = new URL(uri);

  return [
    pathname.replace(/\/$/, ''),
    search,
  ].join('');
}

/**
 * Build a paramString from an the keyValues of an object
 *
 * @param {Object} params
 * @return {string}
 */
export function buildParamString(params) {
  const paramsAsString =
      keys(params)
        .filter(param => params[param])
        .map(param => `${param}=${encodeURIComponent(get(params, param))}`)
        .join('&');
  const preParam = paramsAsString.length ? '?' : '';

  return `${preParam}${paramsAsString}`;
}

/**
 * Build a URL from the base url and given params
 *
 * @param {string} url
 * @param {Object} params
 * @return {string}
 */
export function buildUrl(url, params) {
  return `${url}${buildParamString(params)}`;
}

/**
 * @param {string} params
 * @return {Object}
 */
export const objectifyParams = params => {
  if(!params) {
    return {};
  }

  return params
    .split('&')
    .reduce((acc, param) => {
      const [name, value] = param.split('=');

      acc[name] = value;

      return acc;
    }, {});
};
