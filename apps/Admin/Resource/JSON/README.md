# JSON

## Requirements

- [Fetch](https://fetch.spec.whatwg.org/) implementation ([browser polyfill](https://github.com/github/fetch))

## Generic JSON based API transactions

This module exports an object with the supported methods (GET and POST):

    import JSON from '.';
    
    JSON.get('/fubar');
    JSON.post('/fubar', {});

The above is syntactic sugar for:

    import { request } from './JSON';
    
    json('GET', '/fubar');
    json('POST', '/fubar', {});
