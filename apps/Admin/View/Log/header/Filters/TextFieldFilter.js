import React, { createElement } from 'react';
import { filterStyleSheet } from './Filter.style';
import {
  withStyles,
  GenericTextField,
  Icon,
} from '@mintlab/ui';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.name
 * @param {string} props.value
 * @param {Boolean} props.hasFocus
 * @param {string} props.startAdornmentName
 * @param {Function} props.endAdornmentAction
 * @param {string} props.placeholder
 * @return {ReactComponent}
 */
export const TextFieldFilter = ({
  classes,
  name,
  value,
  hasFocus,
  startAdornmentName,
  endAdornmentAction,
  placeholder,
}) => (
  <div
    className={classes.filterWrapper}
  >
    <GenericTextField
      name={name}
      value={value}
      hasFocus={hasFocus}
      startAdornment={createElement(Icon, {
        size: 'small',
        children: startAdornmentName,
      })}
      closeAction={value ? () => endAdornmentAction(name) : null}
      placeholder={placeholder}
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);
