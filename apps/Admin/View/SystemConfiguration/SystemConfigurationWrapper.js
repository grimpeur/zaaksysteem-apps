import React from 'react';
import Resource from '../Shared/ResourceContainer';
import SystemConfigurationContainer from './SystemConfigurationContainer';

/**
 * Wrap the {@link SystemConfigurationContainer} with a {@link Resource}.
 *
 * @return {ReactElement}
 */
const SystemConfigurationWrapper = ({ route, segments }) => (
  <Resource
    id={['config', 'systemConfiguration']}
  >
    <SystemConfigurationContainer
      route={route}
      segments={segments}
    />
  </Resource>
);

export default SystemConfigurationWrapper;
