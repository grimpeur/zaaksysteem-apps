import { normalize } from './resource';

/**
 * @test {normalize}
 */
describe('The `normalize` function ', () => {
  test('normalizes strings', () => {
    const actual = normalize('fubar');
    const expected = [['fubar', null]];

    expect(actual).toEqual(expected);
  });

  test('normalizes ordered pairs', () => {
    const actual = normalize(['fubar', {}]);
    const expected = [['fubar', {}]];

    expect(actual).toEqual(expected);
  });

  test('normalizes arrays of strings and ordered pairs', () => {
    const actual = normalize([
      'fubar',
      ['fubar', {}],
    ]);
    const expected = [['fubar', null], ['fubar', {}]];

    expect(actual).toEqual(expected);
  });

  test('allows a custom resource descriptor', () => {
    const descriptor = [{
      id: 'fubar',
      method: 'GET',
      url: '/api/',
    }, {}];
    const actual = normalize(descriptor);
    expect(actual).toEqual([descriptor]);
  });

});
