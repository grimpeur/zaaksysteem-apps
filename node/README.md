# `./node`

> This directory is only used for interactive development in the container.

## Coding style

You must use [Node.js modules](https://nodejs.org/docs/latest-v8.x/api/modules.html).
