import {
  extract,
  performGetOnProperties,
  purge,
  filterProperties,
} from './object';

const { assign } = Object;

describe('The `object` module', () => {
  const sourceObject = () => ({
    foo: 'FOO',
    bar: 'BAR',
    quux: 'QUUX',
  });

  /**
   * @test {extract}
   */
  describe('exports an `extract` function that', () => {
    test('can extract a single property with the key as first argument', () => {
      const actual = extract('bar', sourceObject());
      const expected = ['BAR', {
        foo: 'FOO',
        quux: 'QUUX',
      }];

      expect(actual).toEqual(expected);
    });

    test('can extract a single property with the key as last argument', () => {
      const actual = extract(sourceObject(), 'bar');
      const expected = [{
        foo: 'FOO',
        quux: 'QUUX',
      }, 'BAR'];

      expect(actual).toEqual(expected);
    });

    test('can extract multiple properties in any argument order', () => {
      const actual = extract('quux', sourceObject(), 'foo');
      const expected = ['QUUX', {
        bar: 'BAR',
      }, 'FOO'];

      expect(actual).toEqual(expected);
    });

    test('throws if no argument is an object', () => {
      function actual() {
        extract('foo', 'bar');
      }

      expect(actual).toThrow();
    });

    test('throws if more than one argument is an object', () => {
      function actual() {
        extract('quux', sourceObject(), 'foo', sourceObject());
      }

      expect(actual).toThrow();
    });
  });

  describe('exports an `performGetOnProperties` function that', () => {
    test('can perform a get on all the properties', () => {
      const getters = {
        one: 'one',
        two: 'two.second',
        three: 'three.third.tripple',
      };
      const object = {
        one: 'first',
        two: {
          second: 'double',
        },
        three: {
          third: {
            tripple: 'trio',
          },
        },
      };
      const actual = performGetOnProperties(object, getters);
      const expected = {
        one: 'first',
        two: 'double',
        three: 'trio',
      };

      expect(actual).toEqual(expected);
    });
  });

  /**
   * @test {purge}
   */
  describe('exports a `purge` function that', () => {
    test('accepts its arguments in any order and returns an object without the supplied properties', () => {
      const clone = assign({}, sourceObject());
      const actual = purge('foo', clone, 'quux');
      const expected = {
        bar: 'BAR',
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('exports a `filterProperties` function that takes an object', () => {
    test('and returns the object with only the given properties', () => {
      const clone = assign({}, sourceObject());
      const actual = filterProperties(clone, 'foo', 'bar');
      const expected = {
        foo: 'FOO',
        bar: 'BAR',
      };

      expect(actual).toEqual(expected);
    });

    test('and returns the object with only the given properties ignoring non-matching properties', () => {
      const clone = assign({}, sourceObject());
      const actual = filterProperties(clone, 'foo', 'bar', 'mismatch');
      const expected = {
        foo: 'FOO',
        bar: 'BAR',
      };

      expect(actual).toEqual(expected);
    });

    test('and returns an empty object when none of the properties match', () => {
      const clone = assign({}, sourceObject());
      const actual = filterProperties(clone, 'mismatch', 'different');
      const expected = {};

      expect(actual).toEqual(expected);
    });
  });
});
