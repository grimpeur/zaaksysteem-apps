/**
 * @return {JSS}
 */
export const filterStyleSheet = () => ({
  filterWrapper: {
    margin: '0px 27px 0px 7px',
    minWidth: '200px',
    maxWidth: '300px',
    width: '100%',
  },
});
