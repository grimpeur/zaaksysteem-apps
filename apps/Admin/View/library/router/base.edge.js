/*
 * Edge configuration for base segment routing, extends stable.
 */

import stable from './base.iframe';

const { assign } = Object;

const edge = {};

export default assign(stable, edge);
