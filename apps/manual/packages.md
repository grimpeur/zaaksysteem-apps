# Managing `npm` packages

This repository's `npm` packages are separated by **context**:

- **build packages** are used in the *Node.js* runtime environment
  to lint, test and compile client-side code and generate the 
  documentation that you are reading right now
- **vendor packages** contain the client-side code that is processed
  by the build packages

## Whitelist

As a developer, you will occasionally need to add or remove a dependency.

Before you can do that, however, you have to add the new package name to 
the whitelist at `/node/bin/library/whitelist.json`. Here and now, this 
prevents you from accidentally installing wrong – and potentially malicious – 
packages and their transitive – and potentially malicious – dependencies by 
making a simple typo in the CLI.

In the long run this list should be moved to a location outside of the 
project and represent the peer reviewed packages that we as an organization 
trust in terms of quality and security.

## Commands

Managing *build context* packages:

    🐳 zs add build [<@scope>/]<name>
    🐳 zs remove build [<@scope>/]<name>

Managing *vendor context* packages:

    🐳 zs add vendor [<@scope>/]<name>
    🐳 zs remove vendor [<@scope>/]<name>

There is no need to memorize this, you will be interactively guided
through the missing arguments by using any of

    🐳 zs
    🐳 zs add
    🐳 zs add (build|vendor)
 
## Vendor packages

Vendor packages have a couple of special characteristics:

- when adding or removing a package, you are prompted for a
  [*main development stack*](./app.html#main-development-stack)
- dependencies per stack are prebuilt as webpack 
  [`DLL`](https://webpack.js.org/plugins/dll-plugin/) 
  bundles
- the vendor `package.json` contains a `dll` object with the
  *main development stack* identifiers as property names for
  their respective arrays of dependencies

So what automagically happens when you run `🐳 zs add vendor foo` is

- prompt for one or more existing stacks
- add the package name to all dll stacks in `package.json`
- install the package  
- build the vendor bundles

What automagically happens when you run `🐳 zs remove vendor foo` is

- prompt for one or more existing stacks
- remove the package name from slected dll stacks in `package.json`
- if no stacks containing the package are left, uninstall the package  
- build the vendor bundles

## Spoofing packages in the `@mintlab` scope

You can replace `@mintlab` packages that are installed in 
`/opt/zaaksysteem-apps/node/vendor/node_modules/@mintlab`
with development versions:

1. Mount the local source directory as a volume under
   `/opt/zaaksysteem-apps/@mintlab`.
2. Create a spoof handler method for the `mintlabScope` 
   object in `node/bin/library/command.js`  
3. Run `🐳 zs spoof` to
    1. build the package
    2. replace its installation
    3. rebuild the vendor bundles

