import React from 'react';
import classNames from 'classnames';
import { cellStyleSheet } from './cells.style';
import { withStyles } from '@mintlab/ui';
import { createDate, createTime } from './../../../../library/formatIso';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.value
 * @return {ReactComponent}
 */
export const DateCell = ({
  classes: {
    date,
    dateTime,
    dateTimeWrapper,
    time,
  },
  value,
}) => (
  <div className={dateTime}>
    <span
      className={classNames(dateTimeWrapper, date)}
    >{createDate(value)}</span>
    <span
      className={classNames(dateTimeWrapper, time)}
    >{createTime(value)}</span>
  </div>
);

export default withStyles(cellStyleSheet)(DateCell);
